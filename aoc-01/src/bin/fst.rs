use std::io;
use std::io::prelude::*;

struct Change {
    op: char,
    val: i32,
}

fn main() {
    let stdin = io::stdin();
    let mut total = 0;
    let mut changes: Vec<Change> = Vec::new();

    let res = stdin.lock().lines().map(|l| l.unwrap());
    for mut r in res {
       changes.push(Change {
           op: r.remove(0),
           val: r.parse::<i32>().unwrap(),
       });
    }

    for change in changes.into_iter() {
        if change.op == '+' {
            total = total + change.val;
        } else {
            total = total - change.val;
        }
    }
    println!("End result is: {}", total);
}
