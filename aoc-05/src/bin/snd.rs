use std::fs::File;
use std::io::prelude::*;

fn main() {
    let filename = "input.txt";
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();

    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");

    check_all_units(contents.trim());
}

fn opposite_cases(fst: char, snd: char) -> bool {
    let fst_upper = fst.to_uppercase().collect::<Vec<_>>();
    let fst_lower = fst.to_lowercase().collect::<Vec<_>>();
    if fst.is_uppercase() && (snd == fst_lower[0]) ||
        fst.is_lowercase() && (snd == fst_upper[0]) {
            return true;
    }
    false
}

fn check_all_units(line: &str) {
    static ASCII_LOWER: [char; 26] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                                    'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                                    's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

    static ASCII_UPPER: [char; 26] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                                    'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                                    'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    let mut shortest_len = line.len();
    for i in 0..26 {
        println!("Checking character: {}", ASCII_LOWER[i]);
        let mut filtered = filter(String::from(line), ASCII_LOWER[i], ASCII_UPPER[i]);
        let reduced = reduce(filtered); 
        if reduced.len() < shortest_len {
            shortest_len = reduced.len();
        }
    }
    println!("Shortest line is {} chars long.", shortest_len);
}

fn filter(line: String, lower: char, upper: char) -> String {
    let mut chars = line.chars().collect::<Vec<_>>();
    let maxlen = chars.len();
    for i in 0..maxlen {
        let j = maxlen - i - 1;
        if chars[j] == lower || chars[j] == upper {
            chars.remove(j);
        }
    }
    chars.iter().collect::<String>()
}

fn reduce(line: String) -> String {
    let mut contents = line.chars().collect::<Vec<_>>();
    let mut changed = true;
    let mut indexlen = contents.len() - 1;
    while changed {
        changed = false;
        for i in 0..indexlen {
            if opposite_cases(contents[i], contents[i + 1]) {
                contents.remove(i + 1);
                contents.remove(i);
                changed = true;
                indexlen -= 2;
                break;
            }
        }
    }
    contents.into_iter().collect::<String>()
}
