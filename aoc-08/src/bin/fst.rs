use std::fs::File;
use std::io::prelude::*;

struct Node {
    order: usize,
    parent: Box<Node>,
    children: Vec<Node>
}

fn main() {
    let filename = "test_input.txt";
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();
    let mut contents = contents.words();

    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");
}
