use std::fs::File;
use std::io::prelude::*;
use std::fmt;
use std::collections::HashMap;

struct Point {
    x: i32,
    y: i32
}

fn main() {
    let filename = "input.txt";
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();
    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");

    let points: Vec<(usize, Point)> = contents.lines()
        .map(|l|{
            let parts = l.split(',').collect::<Vec<&str>>();
            return Point {
                x: parts[0].trim().parse().unwrap(),
                y: parts[1].trim().parse().unwrap()
            };
        })
        .enumerate()
        .collect();

    let indexed_pts = calc_closest(&points);
    let mut ctr = 0;
    for (index, _) in indexed_pts.iter() {
        print!("{}", index);
        ctr += 1;
        if ctr % 8 == 0 {
            println!("");
        }
    }

    {
        let mut indices: Vec<String> = indexed_pts.iter().map(|(ind, _)| ind.to_string()).filter(|c| c != ".").collect();
        indices.sort_unstable();
        let mut map = HashMap::new();
        for ind in indices.iter() {
            let c = map.entry(ind).or_insert(0);
            *c += 1;
        }
        let max_index = map.iter().map(|(_, v)| v).max().unwrap();
        println!("Maximum repetitions of an element: {}", max_index);
    }
}

fn calc_closest(points: &Vec<(usize, Point)>) -> Vec<(String, Point)> {
    let (top_left, bot_right) = calc_boundaries(points);
    println!("Top left: {}, bottom right: {}", top_left, bot_right);
    let mut indexed_pts: Vec<(String, Point)> = Vec::new();

    for i in top_left.x..bot_right.x {
        for j in top_left.y..bot_right.y {
            let pt = Point {
                x: i,
                y: j
            };
            let closest = pt.manhattan_all(points);
            indexed_pts.push((closest, pt));
        }
    }
    return indexed_pts;
}

fn calc_boundaries(points: &Vec<(usize, Point)>) -> (Point, Point) {
    let mut top_left = Point {
        x: 99999,
        y: 99999
    };
    let mut bot_right = Point {
        x: 0,
        y: 0
    };
    for p in points.iter().map(|(_, pt)| pt) {
        if p.x < top_left.x {
            top_left.x = p.x;
        }
        if p.x < top_left.y {
            top_left.y = p.y;
        }
        if p.x > bot_right.x {
            bot_right.x = p.x;
        }
        if p.y > bot_right.y {
            bot_right.y = p.y;
        }
    }
    (top_left, bot_right)
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl Point {
    fn manhattan_all(&self, pts: &Vec<(usize, Point)>) -> String {
        let mut closest = String::new();
        let mut min_distance = i32::max_value();

        for (index, pt) in pts.iter() {
            let manhattan_distance = self.manhattan_one(pt);
            //println!("Distance from {} to {} is {}", self, pt, manhattan_distance);
            if manhattan_distance < min_distance {
                min_distance = manhattan_distance;
                closest = index.to_string(); 
            } else if manhattan_distance == min_distance {
                closest = ".".to_string();
            }
        }
        return closest;
    }

    fn manhattan_one(&self, pt: &Point) -> i32 {
        (self.x - pt.x).abs() + (self.y - pt.y).abs()
    }
}
